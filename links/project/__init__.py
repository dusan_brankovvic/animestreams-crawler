from flask import Flask, jsonify, request
import os
from Services.AnimeStream import AnimeStream
from Services.Request import Request
import simplejson as json
from bs4 import BeautifulSoup
import cfscrape
# instantiate the app
app = Flask(__name__)
# set config
app_settings = os.getenv('APP_SETTINGS')
app.config.from_object(app_settings)
app.config.from_object('config.DevelopmentConfig')

@app.route('/get/links', methods=['GET'])
def get_direct_link():
    if 'link' not in request.args:
        return jsonify({
            'data':[],
            'error':True
        })

    anime = AnimeStream(request.args['link'], Request())

    return jsonify({
        'data':anime.fetchAllAnimeWatchLinks(),
        'error':False
    })