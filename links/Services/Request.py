import cfscrape
class Request:
    def __init__(self):
        self.scraper = cfscrape.create_scraper()
    def makeRequest(self, url):
        r = self.scraper.get(url)
        if r.status_code > 300:
            return None
        return r.content