import re
from bs4 import BeautifulSoup
import logging
class AnimeStream:
    serverUrl = "http://www1.animestreams.net/ajax-get-link-stream/?server=rapidvideo&filmId="
    def __init__(self, animeUrl, request):
        self.animeUrl = animeUrl
        self.request = request
    def getFullServerUrlWithId(self, episodeId):
        return self.serverUrl + str(episodeId)


    def getEpisodeUrls(self, soup):
        div = soup.find('div', {'id': 'seasonss'})
        if div is None:
            return None

        return div.find_all('a')

    def getEpisodeNumber(self, episodeText):
        number = [int(s) for s in re.findall(r'\b\d+\b', episodeText)]
        if len(number) > 0:
            return number[len(number) -1]
        return None

    def getFilmId(self, soup):
        scripts = soup.find_all('script')
        number = []
        for script in scripts:
            if script.text.find('filmId') != -1:
                number = [int(s) for s in re.findall(r'\b\d+\b', script.text)]
                break
        if len(number) == 0:
            return None
        return number[0]

    def getPaginationLinks(self, soup):
        links = []
        div = soup.find('div', {'id': 'paginationep'})
        if div is None:
            return None
        div.find_all('li')
        for li in div.find_all('li'):
            aLink = li.find('a')
            if aLink.get('href') is not None:
                links.append(aLink.get('href'))
        return links


    def getAllLinkFromPage(self, a):
            episodeContent = self.request.makeRequest(a.get('href'))

            if episodeContent is None: return None

            soup2 = BeautifulSoup(episodeContent)

            url = self.request.makeRequest(self.serverUrl + str(self.getFilmId(soup2)))
            decodedUrl = url.decode("utf-8")
            if decodedUrl.find('http') != -1:
                return decodedUrl.replace('http', 'https')
            else:
                if len(decodedUrl) != 0:
                    return "https:" + url.decode("utf-8")

    def fetchAllAnimeWatchLinks(self):

        data = {}

        content = self.request.makeRequest(self.animeUrl)

        if content is None: return data

        soup = BeautifulSoup(content)

        urls = self.getPaginationLinks(soup)

        allElements = self.getEpisodeUrls(soup)

        for a in allElements:

            number = self.getEpisodeNumber(a.text)

            if number is not None:
                data[number] = self.getAllLinkFromPage(a)

        for url in urls:
            c = self.request.makeRequest(url)

            if c is None: continue

            s = BeautifulSoup(c)

            newElements = self.getEpisodeUrls(s)

            for a in newElements:

                number = self.getEpisodeNumber(a.text)

                if number is not None:
                    data[number] = self.getAllLinkFromPage(a)

        return data
